FROM debian:8.11

MAINTAINER Tricarico Gianni <gianni.tricarico@heh.be>

RUN apt-get update \
&& apt-get install -y sl \
cowsay

WORKDIR /usr/games

COPY animation.sh .

RUN chmod +x animation.sh

ENTRYPOINT ["/usr/games/animation.sh"]
